#include <QObject>
#include <QTcpServer>

class Server : public QTcpServer {
Q_OBJECT
public:
    explicit Server(QObject *parent = nullptr); //
    bool launch();

    std::string db_image_table;

signals:

private:
    QList<QTcpSocket *> m_clients; // лист сокетов клиентов
    std::vector<std::tuple<int, std::string, std::string> > ListOfClient; // лист данных клиентов

private:
    void onNewConnection();

    void readDataFromClient();

};

enum {
    LOAD_IMAGE_TO_DB = 50,
    GET_GALLERY = 60,
    SEND_IMAGE_TO_CLIENT = 70,
    DELETE_IMAGE_FROM_DB = 90,
    UPDATE_IMAGE_IN_DB = 80,
};