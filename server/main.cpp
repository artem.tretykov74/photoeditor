#include <QCoreApplication>
#include "Connection.h"

int main(int argc, char **argv) {
    QCoreApplication a(argc, argv);
    Server server;
    server.launch();
    return a.exec();

}

