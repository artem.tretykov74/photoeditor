//
// Created by user on 12/5/21.
//

#ifndef PHOTOEDITOR_DATABASE_H
#define PHOTOEDITOR_DATABASE_H

#include <string>
#include <vector>
#include <fstream>
#include "sqlite3.h"
#include <nlohmann/json.hpp>


class SQL_clients {
private:
    static int find_clients(void *data, int argc, char *argv[], char *colName[]);

    static int callbackToken(void *data, int argc, char *argv[], char *colName[]);

    sqlite3 *db; // создаём переменную, которая будет указывать на открытую базу данных

protected:

    char *errormess = nullptr; //в это строку будут записываться ошибки при выполнении запроса
    int returncode; // идентификатор базы данных.
    std::string sqlRequest; // SQL запрос.

    char *EM = nullptr;

    static int callback(void *data, int argc, char *argv[],
                        char *colName[]); //функция обратного вызова имеет строгое определение,которое задаётся интерфейсом sqlite3

public:
    SQL_clients();

    ~SQL_clients();

    int InsertFunc(std::vector<std::string> data);

    static int findclients;

    int sql_find(std::vector<std::string> data, std::vector<std::string> col);

    std::string getcallToken(std::vector<std::string> data);

};

class SQL_image : public SQL_clients {
private:
    sqlite3 *db2;

    //выпажение в котором будем компилировать
    sqlite3_stmt *stmt;


public:
    SQL_image();

    ~SQL_image();


    void createImageTable(std::string);


    void Gallery(std::string, int &, std::string &send_to_client);

    void LoadImageFromDB(std::string table, std::string image, std::string ext, std::string &);


    void LoadImageToDB(std::vector<std::string> dataImage);


    void DeleteImageFromDB(std::string table, std::string image, std::string ext);


    void UpdateImageInDB(std::vector<std::string> dataImage);


};

#endif //PHOTOEDITOR_DATABASE_H
