#include <QTcpSocket>
#include <iostream>
#include "Connection.h"
#include "database.h"
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <string>

Server::Server(QObject *parent) : QTcpServer(parent) {
    QObject::connect(this, &QTcpServer::newConnection, this, &Server::onNewConnection);
}

SQL_clients ClientDataBase;
SQL_image ImageBase;

bool Server::launch() {
    if (isListening()) { // Указывает серверу прослушивать входящие соединения по адресу и порту
        std::cout << "Server already listening..." << std::endl;
    }

    if (listen(QHostAddress("127.0.0.1"), 1234)) {
        std::cout << "Server listening..." << std::endl;
    } else {
        std::cout << "Something went wrong" << std::endl;
    }

    return isListening();
}

void Server::onNewConnection() {
    QTcpSocket *client = nextPendingConnection();
    m_clients.append(client);

    QObject::connect(client, &QTcpSocket::readyRead, this, &Server::readDataFromClient);
}


void Server::readDataFromClient() {
    QTcpSocket *client = static_cast<QTcpSocket *>(sender());

    QByteArray data;
    while (client->waitForReadyRead(250));
    data = client->readAll();


   

    bool error_flag = false;
    std::string clientanswer(data.constData(), data.size());
    /*FILE *Logfile = fopen("../server/Log.txt", "a");
    fprintf(Logfile, "%s\n", clientanswer.c_str());
    fclose(Logfile); */
    data.clear();
    std::string error_mes;
    nlohmann::json ClientData;
    try {

        ClientData = nlohmann::json::parse(clientanswer);
    }
    catch (nlohmann::json::parse_error &e) {
        
        error_flag = true;

    }
    if (error_flag) {


        nlohmann::json errorq;

        errorq["idoperation"] = 404;
        errorq["which_error"] = 40422;
        error_mes = errorq.dump();

        client->write(error_mes.data());
        client->waitForBytesWritten();
        error_mes.clear();

    } else {
        int idOperation = ClientData["id_image"];

        
        if (idOperation == 0) {
            int clientToken = ClientData["token"];
            std::string clientLogin = ClientData["login"];
            std::string clientPasswd = ClientData["password"];


            int toReg = ClientData["toReg"];
            int toAuth = ClientData["toAuth"];

            

            if (toReg) {
                ListOfClient.push_back({clientToken, clientLogin, clientPasswd});
                std::stringstream ss;
                ss << clientToken;
                std::string strClToken = ss.str();
                std::vector<std::string> toDataBase = {strClToken,
                                                       clientLogin,
                                                       clientPasswd};
                //имя таблицы

                ClientDataBase.InsertFunc(toDataBase);
                ImageBase.createImageTable(strClToken);

            }
            if (toAuth) {
                std::vector<std::string> toDataBase = {clientLogin, clientPasswd};
                std::vector<std::string> colname = {"login", "password"};

                int ResultOfSearch = ClientDataBase.sql_find(toDataBase, colname);
               


                if (ResultOfSearch) {
                    // Авторизация прошла успешно
                    SQL_clients result_token;
                   
                    int ResultOfToken = std::stoi(result_token.getcallToken(toDataBase));
                    nlohmann::json json;
                    json["token"] = ResultOfToken;
                    json["type"] = 1;

                    std::string request = json.dump();
                    client->write(request.data());
                    client->waitForBytesWritten();
                } else {
                    //Такого пользователя не существует
                    nlohmann::json json;
                    json["type"] = 0;
                    json["token"] = 0;

                    std::string request = json.dump();
                    client->write(request.data());
                    client->waitForBytesWritten();
                }
            }
        }


        switch (idOperation) {
            case LOAD_IMAGE_TO_DB: {
                
                std::string db_image_table = ClientData["name_table"];
                std::string img_name = ClientData["name_file"];
                std::string ext = ClientData["extension"];
                std::string date_created = ClientData["date_add"];
                std::string date_update = ClientData["date_update"];
                std::string size_img = ClientData["size"];
                std::string image = ClientData["image"];
                std::vector<std::string> ImgReqToDB = {db_image_table, img_name, ext, date_created, date_update,
                                                       size_img,
                                                       image};
                ImageBase.LoadImageToDB(ImgReqToDB);
                ImgReqToDB.clear();
                break;
            }

            case GET_GALLERY: {
                db_image_table = ClientData["name_table"];
                std::string send_to_client;
                int numRows = 0;

                ImageBase.Gallery(db_image_table, numRows, send_to_client);
                client->write(send_to_client.data());
                client->waitForBytesWritten();
                break;
            }

            case SEND_IMAGE_TO_CLIENT: {
                db_image_table = ClientData["name_table"];
                std::string name_image = ClientData["name_image"];
                std::string extension = ClientData["extension"];
                std::string send_to_client;
                ImageBase.LoadImageFromDB(db_image_table, name_image, extension, send_to_client);
                client->write(send_to_client.data());
                client->waitForBytesWritten();
                break;
            }
            case UPDATE_IMAGE_IN_DB: {
                db_image_table = ClientData["name_table"];
                std::string name_image = ClientData["name_image"];
                std::string extension = ClientData["extension"];
                std::string date_update = ClientData["date_update"];
                std::string size_img = ClientData["size"];
                std::string image = ClientData["image"];
                std::vector<std::string> UpdateVector = {db_image_table, name_image, extension, date_update,
                                                         size_img,
                                                         image};
                ImageBase.UpdateImageInDB(UpdateVector);
                break;
            }
            case DELETE_IMAGE_FROM_DB: {
                db_image_table = ClientData["name_table"];
                std::string name_image = ClientData["name_image"];
                std::string extension = ClientData["extension"];
                ImageBase.DeleteImageFromDB(db_image_table, name_image, extension);
                break;
            }
        }
    }
}