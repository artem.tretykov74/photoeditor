#include "sqlite3.h"
#include "database.h"
#include <string>
#include <fstream>
#include <time.h>
#include <math.h>
#include <cstring>
#include <assert.h>

using namespace std;


int SQL_clients::findclients;


int SQL_clients::callbackToken(void *data, int argc, char **argv, char **colName) {
    ofstream fout("fileToken.txt", ios_base::out | ios_base::trunc);
    fout << argv[0];
    fout.close();
    return 0;

}

std::string SQL_clients::getcallToken(std::vector<std::string> data) {
    sqlRequest = "SELECT token FROM Clients WHERE login='" + data[0] + "' AND password='" + data[1] + "';";
    returncode = sqlite3_exec(db, sqlRequest.c_str(), callbackToken, 0, &EM);
    if (returncode != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", EM);
        sqlite3_free(EM);
    }
    ifstream fin("fileToken.txt", ios_base::in);

    std::string tok;
    getline(fin, tok);
    fin.close();
    return tok;
}


int SQL_clients::callback(void *data, int argc, char *argv[], char *colName[]) {
    return 0;
}


//создание таюлицы клиентов
SQL_clients::SQL_clients() {
    returncode = sqlite3_open("../server/Database.db", &db); // открытие базы данных
    if (returncode != 0) {
        fprintf(stderr, "Error opening DB: %s\n", sqlite3_errmsg(db));
    }
    sqlRequest = "CREATE TABLE IF NOT EXISTS Clients(token INTEGER, login TEXT PRIMARY KEY NOT NULL, password TEXT NOT NULL); ";
    returncode = sqlite3_exec(db, sqlRequest.c_str(), callback, 0, &errormess);
    if (returncode != SQLITE_OK) {
        fprintf(stderr, "SQLite error: %s\n", errormess);
        sqlite3_free(errormess);
    }
}

SQL_clients::~SQL_clients() {
    sqlite3_close(db);
}


//запись клиента в бд
//мои доработки ?добавил  в аргумент имя личной таблицы пользователя которая будет создаваться в этой функции
int SQL_clients::InsertFunc(std::vector<std::string> data) {

    int errCode = 0;
    sqlRequest = "INSERT INTO Clients (token,login,password) VALUES (";
    for (int i = 0; i < data.size(); i++) {
        sqlRequest += "'" + data[i] + "',";
    }
    sqlRequest.pop_back();
    sqlRequest += ")";
    returncode = sqlite3_exec(db, sqlRequest.c_str(), callback, 0, &errormess);
    if (returncode != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", errormess);
        sqlite3_free(errormess);
        errCode = -1;
    }

    return errCode;
}

int SQL_clients::find_clients(void *data, int argc, char *argv[], char *colName[]) {
    findclients++;
    return 0;
}


//функция поиска существующего клиента
int SQL_clients::sql_find(std::vector<std::string> data, std::vector<std::string> col) {

    findclients = 0;
    sqlRequest = "SELECT * FROM Clients WHERE ";
    for (int i = 0; i < data.size(); i++) {
        sqlRequest += col[i] + "='" + data[i] + "' AND ";
    }
    sqlRequest.erase(sqlRequest.size() - 5, 5);

    sqlRequest += "; ";
    returncode = sqlite3_exec(db, sqlRequest.c_str(), find_clients, 0, &EM);
    if (returncode != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", EM);
        sqlite3_free(EM);
        findclients = -1;
    }

    return findclients;
}


SQL_image::SQL_image() {

    returncode = sqlite3_open("../server/DatabaseImages.db", &db2); // открытие базы данных
    if (returncode != 0) {
        fprintf(stderr, "Error opening DB: %s\n", sqlite3_errmsg(db2));
    }
}

SQL_image::~SQL_image() {
    sqlite3_close(db2);
}


void SQL_image::createImageTable(std::string name_table) {
    sqlRequest = "CREATE TABLE IF NOT EXISTS '" + name_table +
                 "' (name_file TEXT, extension TEXT, date_add TEXT, date_update TEXT, Size INTEGER, image BLOB)";
    returncode = sqlite3_exec(db2, sqlRequest.c_str(), callback, nullptr, &errormess);
    if (returncode != SQLITE_OK) {
        fprintf(stderr, "SQLite error: %s\n", errormess);
        sqlite3_free(errormess);
    }
}

void SQL_image::LoadImageToDB(std::vector<std::string> dataImage) {
    sqlRequest =
            "INSERT INTO '" + dataImage[0] + "' (name_file,extension,date_add,date_update,Size,image) Values ('"
            + dataImage[1] + "', '" + dataImage[2] + "', '" + dataImage[3] + "', '" + dataImage[4] + "', '" +
            dataImage[5] +
            "', '" + dataImage[6] + "');";
    returncode = sqlite3_exec(db2, sqlRequest.c_str(), callback, nullptr, &errormess);
    if (returncode != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", errormess);
        sqlite3_free(errormess);
    }


};


void SQL_image::Gallery(std::string name_table, int &numRows, std::string &send_to_client) {

    sqlRequest = "SELECT * FROM '" + name_table + "' ;";

    nlohmann::json Table_Image;

    returncode = sqlite3_prepare_v2(db2, sqlRequest.c_str(), sqlRequest.size() + 1, &stmt, nullptr);
    assert(returncode == SQLITE_OK);
    while ((returncode = sqlite3_step(stmt)) == SQLITE_ROW) {
        std::string name_image = "name_image" + to_string(numRows);
        Table_Image[name_image]["name_image"] = std::string(
                reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0)));
        Table_Image[name_image]["ext"] = std::string(reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1)));
        Table_Image[name_image]["dadd"] = std::string(reinterpret_cast<const char *>(sqlite3_column_text(stmt, 2)));
        Table_Image[name_image]["dupt"] = std::string(reinterpret_cast<const char *>(sqlite3_column_text(stmt, 3)));
        Table_Image[name_image]["sizeb"] = std::string(reinterpret_cast<const char *>(sqlite3_column_text(stmt, 4)));

        ++numRows;
    }
    assert(returncode == SQLITE_DONE);
    Table_Image["numRows"] = numRows;
    Table_Image["idoperation"] = 60;
    send_to_client = Table_Image.dump();
    sqlite3_finalize(stmt);


}


//функция недоделанная, сам файл должен создаваться на стороне клиента
void SQL_image::LoadImageFromDB(string table, string image, string ext, string &send) {
    sqlRequest = "SELECT image FROM '" + table + "' WHERE name_file = '" + image + "' AND extension = '" +
                 ext + "' ;";
    nlohmann::json ImageToServer;
    ImageToServer["idoperation"] = 70;
    ImageToServer["name_image"] = image + "." + ext;

    returncode = sqlite3_prepare_v2(db2, sqlRequest.c_str(), sqlRequest.size() + 1, &stmt, nullptr);
    assert(returncode == SQLITE_OK);


    while ((returncode = sqlite3_step(stmt)) == SQLITE_ROW) {
        ImageToServer["image"] = std::string(reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0)));
    }
    assert(returncode == SQLITE_DONE);


    send = ImageToServer.dump();
    sqlite3_finalize(stmt);


}

void SQL_image::DeleteImageFromDB(string table, string image, string ext) {
    sqlRequest = "DELETE FROM '" + table + "' WHERE name_file = '" + image + "' AND extension = '" +
                 ext + "' ;";
    returncode = sqlite3_exec(db2, sqlRequest.c_str(), callback, nullptr, &errormess);
    if (returncode != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", errormess);
        sqlite3_free(errormess);
    }
}

void SQL_image::UpdateImageInDB(std::vector<std::string> dataImage) {
    sqlRequest = "UPDATE '" + dataImage[0] + "' SET image= '" + dataImage[5] + "', date_update = '" + dataImage[3] +
                 "', Size = '" + dataImage[4] + "' WHERE name_file= '" + dataImage[1] +
                 "' AND extension = '" + dataImage[2] + "' ;";
    returncode = sqlite3_exec(db2, sqlRequest.c_str(), callback, nullptr, &errormess);
    assert(returncode == SQLITE_OK);

}