#pragma once

#include <QWidget>
#include <QDialog>
#include <QTcpSocket>
#include <QStandardItemModel>
#include <QAbstractItemModel>
#include <QString>
#include <nlohmann/json.hpp>
#include <fstream>
#include "../client/base64/base64.h"

namespace Ui {
    class Form;
}

class Form : public QWidget {
Q_OBJECT

public:

    explicit Form(int token, QString filesave, short saveoperation, QWidget *parent = 0);

    explicit Form(int token, QWidget *parent = 0);

    //explicit Form(int token, QString filename, int saveoperation, QWidget *parent = 0);
    std::string getEnterToken();

    ~Form();

private slots:

    void on_pushButton_9_clicked();

    void on_pushButton_10_clicked();

    void on_pushButton_12_clicked();

    void on_pushButton_clicked();

    void slotLoadImage();

private:
    void delay();

    void delayt();

    Ui::Form *ui;

    QString filesavename = "";
    short saveoperationfile = 0;

    void receiveFromServer();

    void UpdateInDB(QString);


    void Request(int token, std::string login, std::string password, int IDtoReg, int IDtoAuth);


    void sendImage(QString);


    QStandardItemModel *model;
    QModelIndex index;

    enum {
        ERROR = 404, ERROR_PARSE_SERVER = 40422, OPEN_GALLERY = 60, LOAD_IMAGE_FROM_DB = 70
    };
protected:
    int EnterToken;
    QTcpSocket *m_socket;
};
