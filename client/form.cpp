#include "form.h"
#include "ui_form.h"
#include "image.h"
#include <QFileDialog>
#include <QErrorMessage>
#include <QAbstractItemView>
#include <QImageReader>
#include <QFileInfo>
#include <QDateTime>
#include <QDir>
#include <QMessageBox>
#include <QTableWidget>


Form::Form(int token, QString filesave, short saveoperation, QWidget *parent) :
        EnterToken(token),
        filesavename(filesave),
        saveoperationfile(saveoperation),
        QWidget(parent),
        ui(new Ui::Form),
        m_socket(new QTcpSocket(this)) {
    m_socket->connectToHost("127.0.0.1", 1234);
    QObject::connect(m_socket, &QTcpSocket::readyRead, this, &Form::receiveFromServer);
    ui->setupUi(this);
    if (!QDir("../client/MyImages").exists()) {
        QDir().mkdir("../client/MyImages");
    }
    if (saveoperation == 1) {
        UpdateInDB(filesave);
    } else if (saveoperation == 2) sendImage(filesave);

    model = new QStandardItemModel(0, 5, this);

    ui->tableView_3->setModel(model);

    //метод выделения
    ui->tableView_3->setSelectionBehavior(QAbstractItemView::SelectRows);
    //Когда пользователь выбирает элемент,любой уже выбранный становится невыделенным
    ui->tableView_3->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView_3->resizeColumnsToContents();
    ui->tableView_3->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView_3->setEditTriggers(nullptr);
    model->setHeaderData(0, Qt::Horizontal, "Имя");
    model->setHeaderData(1, Qt::Horizontal, "Расширение");
    model->setHeaderData(2, Qt::Horizontal, "Дата создания");
    model->setHeaderData(3, Qt::Horizontal, "Дата изменения");
    model->setHeaderData(4, Qt::Horizontal, "Размер,Б");
    ui->tableView_3->show();
    delayt();
    nlohmann::json getTable;
    getTable["id_image"] = 60;
    getTable["name_table"] = getEnterToken();
    std::string send = getTable.dump();
    m_socket->write(send.data());
    connect(ui->tableView_3, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(slotLoadImage()));
}

Form::Form(int token, QWidget *parent) :
        EnterToken(token),
        QWidget(parent),
        ui(new Ui::Form),
        m_socket(new QTcpSocket(this)) {
    m_socket->connectToHost("127.0.0.1", 1234);
    QObject::connect(m_socket, &QTcpSocket::readyRead, this, &Form::receiveFromServer);
    ui->setupUi(this);
    if (!QDir("../client/MyImages").exists()) {
        QDir().mkdir("../client/MyImages");
    }
    model = new QStandardItemModel(0, 5, this);

    ui->tableView_3->setModel(model);

    //метод выделения
    ui->tableView_3->setSelectionBehavior(QAbstractItemView::SelectRows);
    //Когда пользователь выбирает элемент,любой уже выбранный становится невыделенным
    ui->tableView_3->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView_3->resizeColumnsToContents();
    ui->tableView_3->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView_3->setEditTriggers(nullptr);
    model->setHeaderData(0, Qt::Horizontal, "Имя");
    model->setHeaderData(1, Qt::Horizontal, "Расширение");
    model->setHeaderData(2, Qt::Horizontal, "Дата создания");
    model->setHeaderData(3, Qt::Horizontal, "Дата изменения");
    model->setHeaderData(4, Qt::Horizontal, "Размер,Б");
    ui->tableView_3->show();
    nlohmann::json getTable;
    getTable["id_image"] = 60;
    getTable["name_table"] = getEnterToken();
    std::string send = getTable.dump();
    m_socket->write(send.data());
    connect(ui->tableView_3, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(slotLoadImage()));
}


Form::~Form() {
    delete ui;
}

void Form::receiveFromServer() {
    while (m_socket->waitForReadyRead(100));
    QByteArray responce = m_socket->readAll();
    std::string serveranswer(responce.constData(), responce.size());
    nlohmann::json result;
    try {

        result = nlohmann::json::parse(serveranswer);
    }
    catch (nlohmann::json::parse_error &e) {

        return;

    }
    int idoperation = result["idoperation"];
    switch (idoperation) {
        case OPEN_GALLERY: {
            int rowend = result["numRows"];
            std::vector<std::tuple<std::string, std::string, std::string, std::string, std::string> > sh;
            for (int i = 0; i < rowend; ++i) {
                std::string name_image = "name_image" + std::to_string(i);
                sh.push_back({result[name_image]["name_image"], result[name_image]["ext"], result[name_image]["dadd"],
                              result[name_image]["dupt"], result[name_image]["sizeb"]});


            }
            int row = 0;
            for (auto &element : sh) {

                model->insertRow(row);
                index = model->index(row, 0);
                model->setData(index, std::get<0>(element).c_str());
                index = model->index(row, 1);
                model->setData(index, std::get<1>(element).c_str());
                index = model->index(row, 2);
                model->setData(index, std::get<2>(element).c_str());
                index = model->index(row, 3);
                model->setData(index, std::get<3>(element).c_str());
                index = model->index(row, 4);
                model->setData(index, std::get<4>(element).c_str());
                ++row;
            }
            break;
        }

        case LOAD_IMAGE_FROM_DB: {
            std::string file = result["name_image"];

            std::string image = result["image"];
            std::string path_to_file = "../client/MyImages/" + file;
            std::ofstream fout(path_to_file, std::ofstream::binary);
            std::string decodeImage = base64_decode(image);
            fout << decodeImage;
            fout.close();
            break;


        }
        case ERROR: {
            int which_error = result["which_error"];
            switch (which_error) {
                case ERROR_PARSE_SERVER: {
                    QMessageBox::warning(this, trUtf8("Error"),
                                         trUtf8("Попытайтесь снова"),
                                         QMessageBox::Ok);
                    break;
                }
            }
            break;
        }
    }


}


std::string Form::getEnterToken() {
    int token = EnterToken;
    return std::to_string(token);
}

void Form::sendImage(QString filik) {

    QFileInfo info(filik);
    int FileSize = info.size(); //размер файла
    QString FileExt = info.suffix(); //расширение файла

    QDateTime FileLastMod = info.lastModified(); //дата изменения
    QDateTime FileCreat = info.created(); //дата создания
    std::string file = filik.toStdString();
    std::string shortname = file.erase(file.find_last_of('.')).substr(file.find_last_of('/') + 1);

    std::string fullpath = filik.toStdString();

    if (!QFile("../client/MyImages/" + info.fileName()).exists()) {


        QFile::copy(filik, "../client/MyImages/" + info.fileName());

    }


    nlohmann::json data_image;
    int id_image = 50;
    data_image["id_image"] = id_image;
    data_image["name_table"] = std::to_string(EnterToken);
    data_image["name_file"] = shortname;
    data_image["extension"] = FileExt.toStdString();
    data_image["date_add"] = FileCreat.toString().toStdString();
    data_image["date_update"] = FileLastMod.toString().toStdString();
    data_image["size"] = std::to_string(FileSize);
    std::ifstream fin(fullpath, std::ifstream::binary);
    std::vector<char> bin_img((std::istreambuf_iterator<char>(fin)), std::istreambuf_iterator<char>());
    std::string code_img = base64_encode((unsigned char *) &bin_img[0], (unsigned int) bin_img.size());
    fin.close();
    data_image["image"] = code_img;


    std::string send = data_image.dump();

    m_socket->write(send.data());
    m_socket->waitForBytesWritten();
}

void Form::UpdateInDB(QString filename) {
    delayt();
    QFileInfo info(filename);
    QString FileExt = info.suffix(); //расширение файла
    QString shortname = info.baseName();
    QString ext = info.suffix();
    QDateTime FileLastMod = info.lastModified(); //дата изменения
    int size = info.size();
    std::ifstream fin(filename.toStdString(), std::ifstream::binary);
    std::vector<char> bin_img((std::istreambuf_iterator<char>(fin)), std::istreambuf_iterator<char>());
    std::string code_img = base64_encode((unsigned char *) &bin_img[0], (unsigned int) bin_img.size());
    int id_image = 80;
    nlohmann::json data_to_update;
    data_to_update["id_image"] = id_image;
    data_to_update["name_table"] = std::to_string(EnterToken);
    data_to_update["name_image"] = shortname.toStdString();
    data_to_update["extension"] = ext.toStdString();
    data_to_update["date_update"] = FileLastMod.toString().toStdString();
    data_to_update["size"] = std::to_string(size);
    data_to_update["image"] = code_img;
    std::string sendr = data_to_update.dump();
    m_socket->write(sendr.data());
    m_socket->waitForBytesWritten();
}

void Form::on_pushButton_9_clicked() //open
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open File"), "/home",
                                                    "Image (*.jpg *.png *jpeg);;PNG file (*.png);;JPG file (*.jpg);;JPEG file (*jpeg)");
    QString filik = filename;
    if (!filik.isEmpty())
    {
        QMessageBox::information(this, "Success", "File downloaded");
        sendImage(filik);
    } else {
        QMessageBox::critical(this,"Error","Load was failed");
    }
}

void Form::on_pushButton_10_clicked() //delete
{

    if (ui->tableView_3->isVisible()) {
        int row = ui->tableView_3->selectionModel()->currentIndex().row();
        QString name_image = model->index(row, 0).data().toString();
        QString extension = model->index(row, 1).data().toString();
        QString question = "Вы уверены, что хотите удалить изображение '" + name_image + "." + extension + "' ?";
        if (row >= 0) {
            if (QMessageBox::warning(this, trUtf8("Удаление записи"),
                                     question,
                                     QMessageBox::Yes | QMessageBox::No) == QMessageBox::No) {
                return;
            } else {
                nlohmann::json DeleteData;
                DeleteData["name_table"] = getEnterToken();
                DeleteData["id_image"] = 90;
                index = model->index(row, 0);
                DeleteData["name_image"] = name_image.toStdString();
                index = model->index(row, 1);
                DeleteData["extension"] = extension.toStdString();;
                QString filename = "../client/MyImages/" + name_image + "." + extension;
                QFile(filename).remove();
                std::string delete_to_server = DeleteData.dump();
                m_socket->write(delete_to_server.data());
                m_socket->waitForBytesWritten();

                model->removeRow(row);

            }
        }
    }
}

void Form::on_pushButton_12_clicked() //load
{
    while (model->rowCount() > 0) {
        model->removeRow(0);
    }

    nlohmann::json getTable;
    getTable["id_image"] = 60;
    getTable["name_table"] = getEnterToken();
    std::string send = getTable.dump();
    m_socket->write(send.data());
    m_socket->waitForBytesWritten();
}

void Form::on_pushButton_clicked() //export
{
    QString expDir = QFileDialog::getExistingDirectory(this, tr("Select the directory where you want to export the image"),
                                                    "/home",
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);
    QString expImg_Name;
    QString expImg_Ext;

    if (ui->tableView_3->isVisible()) {
        int row = ui->tableView_3->selectionModel()->currentIndex().row();
        if (row >= 0) {
                index = model->index(row, 0);
                expImg_Name = index.data().toString();
                index = model->index(row, 1);
                expImg_Ext = index.data().toString();
            }
        }
    QString expImg_Path = "../client/MyImages/" + expImg_Name + "." + expImg_Ext;
    QString expFullDestination = expDir + QDir::separator() + expImg_Name + "." + expImg_Ext;
    if ( !(QFile::copy(expImg_Path, expFullDestination)))
    {
        QMessageBox messageBox;
        messageBox.critical(0,"Error","Export failed");
        messageBox.setFixedSize(500,200);
    } else{
        QMessageBox::information(this, "", "Your image successfully exported");
    }

}


void Form::slotLoadImage() {
    QItemSelectionModel *selectModel = ui->tableView_3->selectionModel();
    QString file = model->index(selectModel->selectedRows().first().row(), 0).data().toString();
    QString ext = model->index(selectModel->selectedRows().first().row(), 1).data().toString();
    QString filename = "../client/MyImages/" + file + "." + ext;
    if (!QFile(filename).exists()) {
        nlohmann::json send_to_getImage;
        send_to_getImage["id_image"] = 70;
        send_to_getImage["name_table"] = getEnterToken();
        send_to_getImage["name_image"] = file.toStdString();
        send_to_getImage["extension"] = ext.toStdString();
        std::string getImage = send_to_getImage.dump();
        m_socket->write(getImage.data());
        m_socket->waitForBytesWritten();
        delay();
    }

    this->close();
    Image *image = new Image(EnterToken, this);

    image->loadNewImage(filename);

    // динамически создаем вижеты
    QWidget *oWindow = new QWidget();
    QWidget *eWindow = new QWidget();
    QWidget *menu = new QWidget();

    menu->setWindowFlags(Qt::CustomizeWindowHint |
                         Qt::WindowTitleHint); // Qt::CustomizeWindowHint отключает «умолчальные» хинты (заголовок, кнопки закрытия и т.п.) а Qt::WindowTitleHint добавляет заголовок окна

    QLabel *oLabel = new QLabel(oWindow);
    QLabel *eLabel = new QLabel(eWindow);
    QVBoxLayout menuOrganizer(menu);
    image->setWindow(eLabel, eWindow, oWindow, menu);

    // Устанавливаем названия
    oWindow->setWindowTitle("Original Image");
    eWindow->setWindowTitle("Editing Image");
    menu->setWindowTitle("Options");

    // расставляем окна
    oWindow->move(0, 0);
    eWindow->move(image->width() + 70, 0);
    menu->move(2 * image->width() + 140, 0);

    // помещаем изображение в лейблы
    oLabel->setPixmap(QPixmap::fromImage(image->getOriginal()));
    oLabel->show();
    eLabel->setPixmap(QPixmap::fromImage(image->getEdited()));
    eLabel->show();

    QPushButton *bSave = new QPushButton("Save image");
    menuOrganizer.addWidget(bSave);
    QObject::connect(bSave, &QPushButton::clicked, image, [=]() { image->saveNewImage(filename); });

    QPushButton *Cancel = new QPushButton("Cancel");
    menuOrganizer.addWidget(Cancel);
    QObject::connect(Cancel, &QPushButton::clicked, image, &Image::goToBack);

    QPushButton *bGrayscale = new QPushButton("Grayscale");
    menuOrganizer.addWidget(bGrayscale);
    QObject::connect(bGrayscale, &QPushButton::clicked, image, &Image::grayscale);

    QPushButton *bNegative = new QPushButton("Negative");
    menuOrganizer.addWidget(bNegative);
    QObject::connect(bNegative, &QPushButton::clicked, image, &Image::negative);

    QPushButton *Sepia = new QPushButton("Sepia");
    menuOrganizer.addWidget(Sepia);
    QObject::connect(Sepia, &QPushButton::clicked, image, &Image::sepia);

    QPushButton *bRestore = new QPushButton("Restore Original");
    menuOrganizer.addWidget(bRestore);
    QObject::connect(bRestore, &QPushButton::clicked, image, &Image::restoreOriginal);

    QLabel *Bright = new QLabel("Brightness scroller");
    menuOrganizer.addWidget(Bright);
    QSlider *FirstSlider = new QSlider(Qt::Horizontal);
    FirstSlider->setRange(-50, 50);
    menuOrganizer.addWidget(FirstSlider);
    QObject::connect(FirstSlider, &QSlider::valueChanged, image, [image] { image->adjustBrightness(); });
    QObject::connect(bRestore, &QPushButton::clicked, FirstSlider, [FirstSlider]() { FirstSlider->setValue(0); });

    QLabel *Contrast = new QLabel("Contrast scroller");
    menuOrganizer.addWidget(Contrast);
    QSlider *SecondSlider = new QSlider(Qt::Horizontal);
    SecondSlider->setRange(0, 300);
    SecondSlider->setValue(100);
    menuOrganizer.addWidget(SecondSlider);
    QObject::connect(SecondSlider, &QSlider::valueChanged, image, [image] { image->adjustContrast(); });
    QObject::connect(bRestore, &QPushButton::clicked, SecondSlider, [SecondSlider]() { SecondSlider->setValue(100); });

    oWindow->show();
    eWindow->show();
    menu->show();
}

void Form::delayt() {

    QTime dieTime = QTime::currentTime().addSecs(1);
    while (QTime::currentTime() < dieTime) {

        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }
}
void Form::delay() {
    this->setEnabled(false);
    QTime dieTime = QTime::currentTime().addSecs(5);

    QMessageBox msbox;
    msbox.setText(trUtf8("Загрузка"));
    msbox.setInformativeText(trUtf8("Подождите, фотография загружается"));
    msbox.setWindowFlags(Qt::FramelessWindowHint);
    msbox.setStandardButtons(QMessageBox::NoButton);
    msbox.setIcon(QMessageBox::Information);
    msbox.show();
    QTime::currentTime();
    while (QTime::currentTime() < dieTime) {

        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }


    msbox.close();


}

