#pragma once
#include <iostream>
#include <cmath>
#include <QApplication>
#include <QtWidgets>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QImage>
#include <QColor>
#include <QMenu>
#include "form.h"


class Image : public QWidget
{
    Q_OBJECT

private:
    QImage edited;
    QImage original;
    QLabel *displayLabel;
    QWidget *displayWindow;
    QWidget *openWindow;
    QWidget *sidePanel;
    bool isGrayscaled;
    int enterToken;
    void linearTransformation(double bias, double gain);
    void UpdateInDB(QString);
    void delay();


public:
    explicit Image(int token, QWidget *parent = 0);
    bool loadNewImage(QString fileName);
    void setWindow(QLabel* label, QWidget* edWindow, QWidget*opWindow, QWidget* menu);
    int width();
    QImage getOriginal();
    QImage getEdited();
    void saveNewImage(QString);
    void goToBack();
    void grayscale();
    void negative();
    void sepia();
    void restoreOriginal();
    void adjustBrightness();
    void adjustContrast();

    static short  s_which_change;



    ~Image()
    {
        displayLabel->~QLabel();
        displayWindow->~QWidget();
        delete openWindow;
        delete sidePanel;

    };

};

