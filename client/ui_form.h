//
// Created by user on 1/1/22.
//

#ifndef PHOTOEDITOR_UI_FORM_H
#define PHOTOEDITOR_UI_FORM_H
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form
{
public:

    QLabel *label_3;
    QTableView *tableView_3;
    QWidget *layoutWidget_3;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pushButton_9;
    QPushButton *pushButton_10;
    QPushButton *pushButton_12;
    QPushButton *pushButton;

    void setupUi(QWidget *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QStringLiteral("Form"));
        Form->resize(1168, 734);
        Form->move(410, 150);

        QFont font;
        font.setFamily(QStringLiteral("Trebuchet MS"));
        font.setPointSize(31);
        font.setBold(true);
        font.setWeight(75);

        QFont font1;
        font1.setPointSize(13);


        label_3 = new QLabel(Form);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(370, -20, 411, 121));
        label_3->setFont(font);
        tableView_3 = new QTableView(Form);
        tableView_3->setObjectName(QStringLiteral("tableView_3"));
        tableView_3->setGeometry(QRect(150, 370, 851, 251));
        layoutWidget_3 = new QWidget(Form);
        layoutWidget_3->setObjectName(QStringLiteral("layoutWidget_3"));
        layoutWidget_3->setGeometry(QRect(180, 250, 781, 71));
        horizontalLayout_3 = new QHBoxLayout(layoutWidget_3);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        pushButton_9 = new QPushButton(layoutWidget_3);
        pushButton_9->setObjectName(QStringLiteral("pushButton_9"));
        pushButton_9->setFont(font1);

        horizontalLayout_3->addWidget(pushButton_9);

        pushButton_10 = new QPushButton(layoutWidget_3);
        pushButton_10->setObjectName(QStringLiteral("pushButton_10"));
        pushButton_10->setFont(font1);

        horizontalLayout_3->addWidget(pushButton_10);

        pushButton_12 = new QPushButton(layoutWidget_3);
        pushButton_12->setObjectName(QStringLiteral("pushButton_12"));
        pushButton_12->setFont(font1);

        horizontalLayout_3->addWidget(pushButton_12);

        pushButton = new QPushButton(layoutWidget_3);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setFont(font1);

        horizontalLayout_3->addWidget(pushButton);

        retranslateUi(Form);

        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QWidget *Form)
    {
        Form->setWindowTitle(QApplication::translate("Profile", "Profile", Q_NULLPTR));

        label_3->setText(QApplication::translate("Profile", "\320\222\320\260\321\210\320\270 \320\270\320\267\320\276\320\261\321\200\320\260\320\266\320\265\320\275\320\270\321\217", Q_NULLPTR));
        pushButton_9->setText(QApplication::translate("Profile", "load new file", Q_NULLPTR));
        pushButton_10->setText(QApplication::translate("Profile", "delete", Q_NULLPTR));
        pushButton_12->setText(QApplication::translate("Profile", "update table", Q_NULLPTR));
        pushButton->setText(QApplication::translate("Form", "export", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Form: public Ui_Form {};
} // namespace Ui
#endif //PHOTOEDITOR_UI_FORM_H
