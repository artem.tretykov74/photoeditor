
#ifndef PHOTOEDITOR_TOKEN_GENERATOR_H
#define PHOTOEDITOR_TOKEN_GENERATOR_H

#include <iostream>
#include <ctime>

int token_gen()
{
   srand(time(0)); //srand  дает функции ранд новое начальное значение, а затем с этим числом проводятся многочисленные операции а тайм0 гарантирует что число будет неодинаковым
    return  1 + rand() % 1000 ; //диапазон от 1 до 1000 включительно
}
#endif //PHOTOEDITOR_TOKEN_GENERATOR_H
