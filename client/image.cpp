#include "image.h"
#include "form.h"
#include <QRegExp>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-escape-sequence"
short Image::s_which_change;

Image::Image(int token, QWidget *parent) :
        enterToken(token),
        QWidget(parent) {
    isGrayscaled = false;
    displayLabel = new QLabel();
    displayWindow = new QWidget();
    s_which_change = 0;
}

bool Image::loadNewImage(QString fileName) {
    bool oWorked, eWorked;

    oWorked = original.load(fileName);      // Saves original image for recovery purposes
    eWorked = edited.load(fileName);        // Saves image to be edited at will

    return oWorked && eWorked;
}

void Image::setWindow(QLabel *label, QWidget *edWindow, QWidget *opWindow, QWidget *menu) {
    displayLabel = label;
    displayWindow = edWindow;
    openWindow = opWindow;
    sidePanel = menu;
}

int Image::width() {
    return edited.width();
}

QImage Image::getOriginal() {
    return original;
}

QImage Image::getEdited() {
    return edited;
}


void Image::saveNewImage(QString filename) {


    if (s_which_change != 0) {


        QMessageBox how_save;
        how_save.setText(trUtf8("Сохранение"));
        how_save.setInformativeText(trUtf8("Как сохранить это изображение?"));
        how_save.setIcon(QMessageBox::Question);
        how_save.setWindowFlags(Qt::FramelessWindowHint);
        QAbstractButton *buttonReload = how_save.addButton(trUtf8("Перезаписать"), QMessageBox::ActionRole);
        QAbstractButton *buttonCopy = how_save.addButton(trUtf8("Сохранить как отдельный файл"),
                                                         QMessageBox::ActionRole);
        how_save.setStandardButtons(QMessageBox::Cancel);


        int r = how_save.exec();

        if (r == QMessageBox::Cancel) how_save.close();
        else if (how_save.clickedButton() == buttonCopy) {
            inputimage:
            QString imageName;
            QInputDialog *popup = new QInputDialog(NULL, NULL);
            bool read;
            QRegExp re(R"(^[a-zA-ZА-Яа-я0-9_\s\t\-]+\.(png|jpg|jpeg)$)");
            do {


                imageName = popup->getText(NULL, "Save Image As",
                                           trUtf8("Введите имя файла для сохранения с расширением(png, jpg, jpeg):"), QLineEdit::Normal,
                                           "", &read);       // Read user input from window

                if (read && imageName.isEmpty())
                    QMessageBox::information(0, "ERROR", trUtf8("Введите имя файла!"));
            } while (read && imageName.isEmpty());

            if (!re.exactMatch(imageName) && read) {
                QMessageBox::information(0, "ERROR", trUtf8("Введено некорректное имя файла!\nУбедитесь что в нем указали верное расширение."));
                goto inputimage;
            } else if (read) {
                if (QFile("../client/MyImages/" + imageName).exists()) {
                    QMessageBox qmes;
                    qmes.setWindowFlags(Qt::FramelessWindowHint);
                    qmes.setInformativeText(trUtf8("Такой файл уже сущесвует, хотите его перезаписать?"));
                    qmes.setIcon(QMessageBox::Information);
                    QAbstractButton *reload = qmes.addButton(trUtf8("Да"), QMessageBox::YesRole);
                    QAbstractButton *cancel = qmes.addButton(trUtf8("Нет"), QMessageBox::NoRole);
                    qmes.exec();
                    if (qmes.clickedButton() == reload) {
                        s_which_change = 0;
                        edited.save("../client/MyImages/" + imageName, 0, -1);

                        QString fil = "../client/MyImages/" + imageName;
                        Form *form;
                        form = new Form(enterToken, fil, 1);
                        form->show();
                        while (displayWindow->isVisible() || openWindow->isVisible() || sidePanel->isVisible()) {
                            displayWindow->close();
                            openWindow->close();
                            sidePanel->close();
                        }
                    } else goto inputimage;
                } else {
                    s_which_change = 0;
                    edited.save("../client/MyImages/" + imageName, 0, -1);
                    QString fil = "../client/MyImages/" + imageName;
                    Form *form;
                    form = new Form(enterToken, fil, 2);
                    form->show();

                    while (displayWindow->isVisible() || openWindow->isVisible() || sidePanel->isVisible()) {
                        displayWindow->close();
                        openWindow->close();
                        sidePanel->close();
                    }


                }
            }


        } else {
            edited.save(filename, 0, -1);
            Form *form;
            form = new Form(enterToken, filename, 1);
            form->show();


            while (displayWindow->isVisible() || openWindow->isVisible() || sidePanel->isVisible()) {
                displayWindow->close();
                openWindow->close();
                sidePanel->close();
            }


        }


    } else this->setEnabled(false);


}





void Image::goToBack() {

    while (displayWindow->isVisible() || openWindow->isVisible() || sidePanel->isVisible()) {
        displayWindow->close();
        openWindow->close();
        sidePanel->close();
    }
    Form *form = new Form(enterToken);
    form->show();
}

void Image::grayscale() {
    s_which_change = 1;
    if (isGrayscaled)
        return;

    for (int i = 0; i < edited.width(); i++) {
        for (int j = 0; j < edited.height(); j++) {
            //  Read, recalculate and set a value for all channels
            QColor pixelValue;
            pixelValue = edited.pixel(i, j);

            int grayValue = 299 * pixelValue.red() + 587 * pixelValue.green() + 114 *
                                                                                pixelValue.blue(); // метод лмнейного приближения вычисление оттенка серого для каждого пиксееля
            grayValue = static_cast<int>(round(grayValue / 1000.0)); // необходимая последующая оптимизация
            pixelValue.setRed(grayValue);
            pixelValue.setGreen(grayValue);
            pixelValue.setBlue(grayValue);

            edited.setPixel(i, j, pixelValue.rgb());
        }
    }
    displayLabel->setPixmap(QPixmap::fromImage(edited)); //показать изменения
    isGrayscaled = true;
}

float clamp(float value, float min, float max) {
    if (value < min)
        return min;
    else if (value > max)
        return max;

    return value;
}

void Image::linearTransformation(double bias, double gain) {
    int w = edited.width(), h = edited.height();

    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            QColor pixelValue;
            int newValue;
            pixelValue = edited.pixel(i, j);

            newValue = static_cast<int>(gain * pixelValue.red() + bias);
            newValue = static_cast<int>(clamp(newValue, 0, 255));
            pixelValue.setRed(newValue);

            newValue = static_cast<int>(gain * pixelValue.green() + bias);
            newValue = static_cast<int>(clamp(newValue, 0, 255));
            pixelValue.setGreen(newValue);

            newValue = static_cast<int>(gain * pixelValue.blue() + bias);
            newValue = static_cast<int>(clamp(newValue, 0, 255));
            pixelValue.setBlue(newValue);

            edited.setPixel(i, j, pixelValue.rgb());
        }
    }
}

void Image::negative() {
    s_which_change = 1;
    linearTransformation(255, -1);
    displayLabel->setPixmap(QPixmap::fromImage(edited));
}

void Image::sepia() {
    s_which_change = 1;
    for (int i = 0; i < edited.width(); i++) {
        for (int j = 0; j < edited.height(); j++) {
            //  Read, recalculate and set a value for all channels
            QColor pixelValue;
            pixelValue = edited.pixel(i, j);

            int RedValue = 0.393 * pixelValue.red() + 0.769 * pixelValue.green() + 0.189 *
                                                                                   pixelValue.blue(); // метод лмнейного приближения вычисление оттенка серого для каждого пиксееля
            int GreenValue = 0.349 * pixelValue.red() + 0.686 * pixelValue.green() + 0.168 * pixelValue.blue();
            int BlueValue = 0.272 * pixelValue.red() + 0.534 * pixelValue.green() + 0.131 * pixelValue.blue();

            RedValue = static_cast<int>(round(RedValue)); // необходимая последующая оптимизация
            GreenValue = static_cast<int>(round(GreenValue)); // необходимая последующая оптимизация
            BlueValue = static_cast<int>(round(BlueValue)); // необходимая последующая оптимизация

            if (RedValue > 255) {
                RedValue = 255;
            }

            if (GreenValue > 255) {
                GreenValue = 255;
            }

            if (BlueValue > 255) {
                BlueValue = 255;
            }

            pixelValue.setRed(RedValue);
            pixelValue.setGreen(GreenValue);
            pixelValue.setBlue(BlueValue);

            edited.setPixel(i, j, pixelValue.rgb());
        }
    }
    displayLabel->setPixmap(QPixmap::fromImage(edited)); //показать изменения
}

void Image::restoreOriginal() {

    edited = original;
    displayLabel->setPixmap(QPixmap::fromImage(edited));
    displayLabel->setGeometry(0, 0, edited.width(), edited.height());
    displayWindow->setGeometry(original.width() + 70, 0, edited.width(), edited.height());
    isGrayscaled = false;
    s_which_change = 0;
}

void Image::adjustBrightness() {

    s_which_change = 1;
    int bias;
    int var = qobject_cast<QSlider *>(sender())->value();
    QString input = QString::number(var);

    if (input == nullptr)
        return;

    bias = input.toInt();
    bias = static_cast<int>(clamp(bias, -255, 255));

    linearTransformation(static_cast<double>(bias), 1);
    displayLabel->setPixmap(QPixmap::fromImage(edited));
}

void Image::adjustContrast() {

    s_which_change = 1;

    double gain;

    double var = qobject_cast<QSlider *>(sender())->value();

    double var1 = var / 100;
    QString input = QString::number(var1);
    if (input == nullptr)
        return;

    gain = input.toDouble();
    gain = clamp(gain, 0.001, 255.0);

    linearTransformation(0, gain);
    displayLabel->setPixmap(QPixmap::fromImage(edited));
}

#pragma clang diagnostic pop