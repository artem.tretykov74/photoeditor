 #include "mainwindow.h"
#include "ui_mainwindow.h"
#include "form.h"
#include <QMessageBox>
#include <nlohmann/json.hpp>
#include "sha.h"
#include "token_generator.h"

 MainWindow::MainWindow(QWidget *parent)
         : QMainWindow(parent)
         , ui(new Ui::MainWindow)
         , m_socket(new QTcpSocket(this))
 {
     ui->setupUi(this);
     m_socket->connectToHost("127.0.0.1", 1234);
     QObject::connect(m_socket, &QTcpSocket::readyRead, this, &MainWindow::receiveFromServer);
     QObject::connect(ui->pushButton, &QPushButton::clicked, this, &MainWindow::sendToServerAuth);
     QObject::connect(ui->pushButton_2, &QPushButton::clicked, this, &MainWindow::sendToServerReg);
 }

 MainWindow::~MainWindow()
 {
     delete ui;
 }
 void MainWindow::Request(int token, std::string login, std::string password, int IDtoReg, int IDtoAuth)
 {
     nlohmann::json data;
     data["token"] = token;
     data["login"] = login;
     data["password"] = password;
     data["toReg"] = IDtoReg;
     data["toAuth"] = IDtoAuth;
     data["id_image"] = 0;
     std::string request = data.dump();
     m_socket->write(request.data());

 }

 void MainWindow::sendToServerAuth()
 {
     QString login = ui ->lineEdit->text();
     QString password = ui ->lineEdit_2->text();
     if (!login.isEmpty() && !password.isEmpty()) {
         std::string clients_login = login.toStdString();
         std::string passwd = password.toStdString();

         std::string username = "2020-3-21-tre";
         std::string shapasswd = sha256(passwd + sha256(username));

         Request(0,clients_login,shapasswd,0,1);
     } else
     {
         QMessageBox::information(this, "Error", "Пожалуйста заполните поля");
     }
 }

 void MainWindow::sendToServerReg()
 {
     QString login = ui ->lineEdit->text();
     QString password = ui ->lineEdit_2->text();
     if (!login.isEmpty() && !password.isEmpty()) {
         std::string clients_login = login.toStdString();
         std::string passwd = password.toStdString();

         std::string username = "2020-3-21-tre";
         std::string shapasswd = sha256(passwd + sha256(username));

         RegToken = token_gen();

         Request(RegToken,clients_login,shapasswd,1,0);
         this -> close();
         Form *form = new Form(RegToken);
         form->show();
     } else
     {
         QMessageBox::information(this, "Error", "Пожалуйста заполните поля");
     }
 }

 void MainWindow::receiveFromServer()
 {
     QByteArray responce = m_socket->readAll();
     std::string serveranswer(responce.constData(), responce.length());
     nlohmann::json result = nlohmann::json::parse(serveranswer);
     //как то получить токен
     int ID = result["type"];
     AuthToken = result["token"];
     if (ID)
     {
         this -> close();
         Form *form = new Form(AuthToken);
         form->show();
     }
     else{
         QMessageBox::information(this, "Error", "Указанный ID не найден");
     }
 }