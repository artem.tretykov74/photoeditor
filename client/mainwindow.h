#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>

class QTcpSocket;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QTcpSocket *m_socket;
    int RegToken;
    int AuthToken;

private:
    void Request(int token, std::string login, std::string password, int IDtoReg, int IDtoAuth);
    void sendToServerAuth();
    void sendToServerReg();
    void receiveFromServer();
};

#endif // MAINWINDOW_H
