//
// Created by user on 12/19/21.
//

#ifndef PHOTOEDITOR_BASE64_H
#define PHOTOEDITOR_BASE64_H
//
//  base64 encoding and decoding with C++.
//  Version: 2.rc.08 (release candidate)
//
#include <string>
std::string base64_decode(std::string const& s, bool remove_linebreaks = false);
std::string base64_encode(unsigned char const*, size_t len, bool url = false);
#endif //PHOTOEDITOR_BASE64_H
