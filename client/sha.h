

#ifndef PHOTOEDITOR_SHA_H

#include <openssl/sha.h>

#include <sstream>
#include <iomanip>



std::string sha256(const std::string str)
{
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, str.c_str(), str.size());
    SHA256_Final(hash, &sha256);
    std::stringstream ss; //потоковый класс
    for(int i = 0; i < SHA256_DIGEST_LENGTH; i++)
    {
        ss << std::hex << std::setw(2) << std::setfill('0') << (int)hash[i];
    }
    return ss.str();
}
#endif //PHOTOEDITOR_SHA_H
//hex  переводить всё числа, которые идут после него в шестнадцатиричную систему исчисления.
//setw задаёт разную ширину поля вывода строк
//Задает символ, который будет использоваться для заполнения пробелов